import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute, RouterModule, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { EmbedVideoService } from 'ngx-embed-video';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import $ from "jquery";
import Player from "@vimeo/player";

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
  providers: [NgbAccordionConfig]
})
export class VideoPlayerComponent implements OnInit {
	bookList = [];
	currentChapter = "Select Lesson";
	currentLesson = "";
	currentVideoName = "&nbsp;";
	currentVideo: any = "";
	jScript: any = "";
	player: any = "";
	iframeId = 1;
  constructor(
  	private sanitizer: DomSanitizer, 
  	private router: Router,
  	private route:ActivatedRoute,
  	private embedService: EmbedVideoService,
  	accConfig: NgbAccordionConfig) { 

  	console.log(accConfig);
  }

  ngOnInit(): void {
  	console.log(this.route.snapshot.paramMap.get('chapter'));
  	this.bookList = [
  		{
  			"name":"Chapter 1",
  			"slug":"chapter-1",
  			"is_completed":false,
  			"lesson":[
	  			{
	  				"name":"Angular",
	  				"slug":"angular",
	  				"video":"https://vimeo.com/111117420",
	  				"is_completed":false,
	  				"time_to_read":"5min"
	  			},
	  			{
	  				"name":"Django/Python",
	  				"slug":"django-python",
		  			"video":"https://vimeo.com/35527420",
		  			"is_completed":false,
		  			"time_to_read":"10min"
	  			},
	  			{
	  				"name":"Laravel",
	  				"slug":"laravel",
	  				"video":"https://vimeo.com/45417023",
	  				"is_completed":true,
	  				"time_to_read":"15min"
	  			}
  			]
  		},
  		{
  			"name":"Chapter 2",
  			"slug":"chapter-2",
  			"is_completed":false,
  			"lesson":[
	  			{
	  				"name":"HTML",
	  				"slug":"html",
	  				"video":"https://vimeo.com/380886323",
	  				"is_completed":false,
	  				"time_to_read":"5min"
	  			},
	  			{
	  				"name":"CSS",
	  				"slug":"css",
		  			"video":"https://vimeo.com/54990931",
		  			"is_completed":false,
		  			"time_to_read":"10min"
	  			},
	  			{
	  				"name":"jQuery",
	  				"slug":"jquery",
	  				"video":"https://vimeo.com/102553634",
	  				"is_completed":false,
	  				"time_to_read":"15min"
	  			}
  			]
  		},
  		{
  			"name":"Chapter 3",
  			"slug":"chapter-3",
  			"is_completed":false,
  			"lesson":[
	  			{
	  				"name":"Angular",
	  				"slug":"angular",
	  				"video":"https://vimeo.com/111117420",
	  				"is_completed":false,
	  				"time_to_read":"5min"
	  			},
	  			{
	  				"name":"Django/Python",
	  				"slug":"django-python",
		  			"video":"https://vimeo.com/35527420",
		  			"is_completed":false,
		  			"time_to_read":"10min"
	  			},
	  			{
	  				"name":"Laravel",
	  				"slug":"laravel",
	  				"video":"https://vimeo.com/45417023",
	  				"is_completed":false,
	  				"time_to_read":"15min"
	  			}
  			]
  		}
  	];

  	this.bookList.forEach(chapter => {
    	if (chapter.slug == this.route.snapshot.paramMap.get('chapter')) {
    		
    		this.currentChapter = chapter.name;

    		chapter.lesson.forEach(lesson => {
    			if (lesson.slug == this.route.snapshot.paramMap.get('lesson')) {
    				this.currentLesson = lesson.name;

    				if (this.getVimeoId(lesson.video)) {
							var v = this.getVimeoId(lesson.video);
							this.currentVideo = this.sanitizer.bypassSecurityTrustResourceUrl(v);
						}

						setTimeout(function() {
						 	var iframe = document.querySelector('iframe');
							this.player = new Player(iframe);
						  this.player.getVideoTitle().then(function(title) {
						    console.log('title:', title);
						    this.currentVideoName = title;
						  }.bind(this));

						  this.player.on('play', function() {
						    console.log('played the video!');
						  });

						 	this.player.on('ended', function() {
						    console.log('Finished');
						    $('#lesson_block_'+lesson.slug).addClass('active');
								$('#lesson_block_'+lesson.slug).find("input[type='checkbox']").prop("checked", "checked");

						    // $(event.target).parents('.list-group-item').find("input[type='checkbox']").prop("checked", "checked");
						    // Ajax call here to update database;
						  });
						}.bind(this), 1000);
    			}
    		});
    	}
    	this.iframeId++;
    });
  	
  	this.router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationStart) {
          // start
      }

      if (event instanceof NavigationEnd) {
	      // Traverse the active route tree
	      // Get current Component active
	      this.bookList.forEach(chapter => {
	      	if (chapter.slug == this.route.snapshot.paramMap.get('chapter')) {
	      		
	      		this.currentChapter = chapter.name;

	      		chapter.lesson.forEach(lesson => {
	      			if (lesson.slug == this.route.snapshot.paramMap.get('lesson')) {
	      				this.currentLesson = lesson.name;

	      				if (this.getVimeoId(lesson.video)) {
									var v = this.getVimeoId(lesson.video);
									this.currentVideo = this.sanitizer.bypassSecurityTrustResourceUrl(v);
								}

								setTimeout(function() {
								 	var iframe = document.querySelector('iframe');
									this.player = new Player(iframe);
								  this.player.getVideoTitle().then(function(title) {
								    console.log('title:', title);
								    this.currentVideoName = title;
								  }.bind(this));

								  this.player.on('play', function() {
								    console.log('played the video!');
								  });

								 	this.player.on('ended', function() {
								    console.log('Finished');
								    // $(event.target).parents('.list-group-item').find("input[type='checkbox']").prop("checked", "checked");
								    // Ajax call here to update database;
								  });
								}.bind(this), 1000);
	      			}
	      		});
	      	}
	      	this.iframeId++;
	      });
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator
          // Present error to user
          // console.log(event.error);
      }
    });
  }

  ngAfterViewInit() {

  }

  playVideo(event:any, book, lesson) {
  	this.player = "";
  	this.currentChapter = book;
		this.currentLesson = lesson.name;
		if (this.getVimeoId(lesson.video)) {
			var v = this.getVimeoId(lesson.video);
			this.currentVideo = this.sanitizer.bypassSecurityTrustResourceUrl(v);
			// $(document).find('#video_player').attr('src', this.sanitizer.bypassSecurityTrustResourceUrl(v));
		}
		
		setTimeout(function() {
			var iframe = document.querySelector('iframe');
			this.player = new Player(iframe);

      this.player.getVideoTitle().then(function(title) {
        console.log('title:', title);
        this.currentVideoName = title;
      }.bind(this));

			this.player.play().then(function() {
				console.log(this.player);
				// $('#lesson_block_'+lesson.slug).addClass('active');
				// $('#lesson_block_'+lesson.slug).find("input[type='checkbox']").prop("checked", "checked");
			}).catch(function(error) {
				// error if any
			});

      this.player.on('play', function() {
        console.log('played the video!');
      });

		 	this.player.on('ended', function() {
        console.log('Finished');
        $(event.target).parents('.list-group-item').addClass('active');
        $(event.target).parents('.list-group-item').find("input[type='checkbox']").prop("checked", "checked");

        setTimeout(function() {
        	alert('completed');
        	window.location.reload();
    		}.bind(this), 2000);

        // Ajax call here to update database;
      }.bind(this));
			this.iframeId++;
		}.bind(this), 1000);
  }

  getVimeoId(url, id = false) {
		var regExp = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
		var match = url.match(regExp);
		if (match){
	  	if (id) {
	  		return match[3];
	  	}
			return "https://player.vimeo.com/video/"+match[3]+"?api=1&player_id=video_player_"+this.iframeId+"&title=0&byline=0&portrait=0";
		}
		return "";
  }
}
