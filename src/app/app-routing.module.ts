import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoPlayerComponent } from './video-player/video-player.component';

const routes: Routes = [
	{
	    path: 'video-player/:chapter/:lesson',
	    component: VideoPlayerComponent,
	    data: { title: 'Video Player' }
	},
	{
	    path: 'video-player/:chapter',
	    component: VideoPlayerComponent,
	    data: { title: 'Video Player' }
	},
	{
	    path: 'video-player',
	    component: VideoPlayerComponent,
	    data: { title: 'Video Player' }
	},
	{
	    path: '',
	    component: VideoPlayerComponent,
	    data: { preload: true, title : 'Video Player' }
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
